﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCBoostrapAlerts.Controllers
{
    public class AlertsController : BaseController
    {
        // GET: Alerts
        public ActionResult Index()
        {
            //Auto Disappear-Click to close
            Success("Success-Auto Disappear-Click to close", true, true);
            Information("Information-Auto Disappear-Click to close", true, true);
            Warning("Warning-Auto Disappear-Click to close", true, true);
            Danger("Danger-Auto Disappear-Click to close", true, true);

            //Auto Disappear-Can not Close
            Success("Success-Auto Disappear-Can not Close", true,false);
            Information("Information-Auto Disappear-Can not Close", true,false);
            Warning("Warning-Auto Disappear-Can not Close", true,false);
            Danger("Danger-Auto Disappear-Can not Close", true,false);


            //Not Disappear-Click to close
            Success("Success-Not Disappear-Click to close", false,true);
            Information("Information-Not Disappear-Click to close", false, true);
            Warning("Warning-Not Disappear-Click to close", false, true);
            Danger("Danger-Not Disappear-Click to close", false, true);

            return View();
        }
    }//end class
}//end namespace