﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Security.Principal;
using System.Web.Routing;
using MVCBoostrapAlerts.Models;
namespace MVCBoostrapAlerts.Controllers
{
    public class BaseController : Controller
    {
        readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        public BaseController()
        {
           
          
        }

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
           
            //if (!User.Identity.IsAuthenticated)
            //{
            //    Logger.Info("Not Authenticated " + requestContext.HttpContext.Request.RawUrl);
            //    HttpContext.Response.Redirect("~/Account/Login", true);
            //    return;
            //}
            //
           
        }

        public void Success(string message, bool dismissable = false)
        {
            AddAlert(AlertStyles.Success, message, dismissable);
        }

        public void Success(string message, bool autoDisappear, bool dismissable = false)
        {
            AddAlert(AlertStyles.Success, autoDisappear, message, dismissable);
        }

        public void Information(string message, bool dismissable = false)
        {
            AddAlert(AlertStyles.Information, message, dismissable);
        }

        public void Information(string message, bool autoDisappear, bool dismissable = false)
        {
            AddAlert(AlertStyles.Information, autoDisappear, message, dismissable);
        }

        public void Warning(string message, bool dismissable = false)
        {
            AddAlert(AlertStyles.Warning, message, dismissable);
        }

        public void Warning(string message, bool autoDisappear, bool dismissable = false)
        {
            AddAlert(AlertStyles.Warning, autoDisappear, message, dismissable);
        }

        public void Danger(string message, bool dismissable = false)
        {
            AddAlert(AlertStyles.Danger, message, dismissable);
        }

        public void Danger(string message, bool autoDisappear, bool dismissable = false)
        {
            AddAlert(AlertStyles.Danger, autoDisappear, message, dismissable);
        }

        private void AddAlert(string alertStyle, string message, bool dismissable)
        {
            var alerts = TempData.ContainsKey(Alert.TempDataKey)
                ? (List<Alert>)TempData[Alert.TempDataKey]
                : new List<Alert>();

            alerts.Add(new Alert
            {
                AlertStyle = alertStyle,
                Message = message,
                Dismissable = dismissable,
                AutoDisappear=false
            });

            TempData[Alert.TempDataKey] = alerts;
           
        }

        private void AddAlert(string alertStyle, bool autoDisappear, string message, bool dismissable)
        {
            var alerts = TempData.ContainsKey(Alert.TempDataKey)
                ? (List<Alert>)TempData[Alert.TempDataKey]
                : new List<Alert>();

            alerts.Add(new Alert
            {
                AlertStyle = alertStyle,
                Message = message,
                Dismissable = dismissable,
                AutoDisappear = autoDisappear
            });

            TempData[Alert.TempDataKey] = alerts;

        }
    
    }//end class
}//end namespace

//Success(string.Format("<b>{0}</b> was successfully added to the database.", person.FirstName), true);
//Danger("Looks like something went wrong. Please check your form.");